# memo-app
## TODO

* [x] #1: Login/Logout text update
* [ ] #2: Fix removing memo causing table to break
* [x] #3: Fix wrong password alert not appearing on first attempt
* [x] #4: Hide memos when not logged in
* [x] #5: Fix "loading data" going indefinitely after first login but fixes on refresh
  -> Cause: Component (re)mount does not occur after login on app.js
* [ ] #6: Adjust visual style
* [ ] #7: Add register option
* [ ] #8: Maybe add some mongodb schema stuff
* [ ] #9: upload to git