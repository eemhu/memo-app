import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import "./Header.css";
import Login from './Login';

/**
 * Header React.Component
 * Props:
 * this.props.onLogin(boolean)
 * this.props.isLoggedIn : boolean
 */
export default class Header extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            viewLogin: false
        };

        this.handleShowLoginModal = this.handleShowLoginModal.bind(this);
    }

    handleShowLoginModal() {
        const currentState = this.state.viewLogin;
        this.setState({
            viewLogin: !currentState
        });
    }

    render() {
        return (
            <React.Fragment>
                <Login onLogin={(login) => this.props.onLogin(login)} isShown={this.state.viewLogin} onToggleVisibility={this.handleShowLoginModal} isLoggedIn={this.props.isLoggedIn}/>
                <Navbar className="mainNavbar" bg="dark" variant="dark" expand="lg">
                    <Navbar.Brand href="#home">
                        memo-app
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    <Navbar.Collapse>
                        <Nav className="mr-auto">
                            <Nav.Link>Home</Nav.Link>
                            <Nav.Link>Memos</Nav.Link>
                            <Nav.Link onClick={() => this.handleShowLoginModal()}>{this.props.isLoggedIn ? "Log out" : "Log in"}</Nav.Link>
                            <Nav.Link>Register</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </React.Fragment>
        );
    }
}