import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import React from 'react';
import MemoTable from './MemoTable';
import Header from './Header';

import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';

const ENABLE_SERVERLESS_OPERATION = false;

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: ENABLE_SERVERLESS_OPERATION ? [{"_id": "1", "title":"wow", "content":"yo"},{"_id": "2", "title":"wow2", "content":"yo2"}] : null,
      isLoggedIn: false
    };
  }



  onSubmit(e) { 
    console.log("app.js/OnSubmit activated");
    e.preventDefault();

    const formData = new FormData(e.target);
    const body = {};

    formData.forEach((value, property) => body[property] = value);
    //console.table(body);

    fetch('./post-memo', {
      method: 'POST',
      headers: {
        'Content-Type' : 'application/json',
      },
      body: JSON.stringify(body),
    })
    .then(response => response.json())
    .then(data => {
      //console.log(data);
      this.getMemos();
    })
    .catch(err => console.log(err));
  }

  getMemos() {
    console.log("Getting memos");

    fetch('./get-memos', {
      method: 'GET',
      headers: {
        'Content-Type' : 'application/json',
      },

    })
    .then(response => response.json())
    .then(data => {
      if (data.auth) {
        let newState = {...this.state};
        newState.data = data.data;
        this.setState(newState); // do with state I guess ( set it and have this.state.data on render)
      }
    })
    .catch(err => {
      console.log(err);
    });
  }

  removeMemo(id) {
    console.log("RemoveMemo activated");
    const body = {
      _id: id
    };

    fetch('./remove-memo', {
      method: 'POST',
      headers: {
        'Content-Type' : 'application/json',
      },
      body: JSON.stringify(body),
    })
    .then(response => response.json())
    .then(data => {
      //console.log(data);
      this.getMemos();
    })
    .catch(err => console.log(err));
  }

  modifyMemo(id) {
      // TODO
  }




  handleClick(action, id) {
    console.log(`Action: ${action}, id: ${id}`);

    switch (action) {
        case "remove":
            this.removeMemo(id);
            break;
        case "modify":
            this.modifyMemo(id);
            break;
        default:
            break;
    }
  }

  handleLogin(login) {
    let newState = {...this.state}; // copy of this.state object with spread operator, could also try json.parse(json.stringify(this.state))
    // making the change to login
    newState.isLoggedIn = login;
    // setting newState as the current state
    this.setState(newState);

    // fetch notes [For fixing issue #5]
    //this.checkLogin();
    if (login) this.getMemos();
  }





  // send GET to /authchk
  // returns json { auth : boolean, username : string/null }
  checkLogin() {
    console.log("Checking for login");

    fetch('./authchk', {
      method: 'GET'
    })
    .then(response => response.json())
    .then(data => {
      this.handleLogin(data.auth);
    })
    .catch(err => console.log(err));
  }





  componentDidMount() {
     if (!ENABLE_SERVERLESS_OPERATION) {
      console.log("<App.js: componentDidMount()> Component mounted!")
       this.checkLogin();
       if (this.state.isLoggedin) this.getMemos();
     }
  }

  renderIsNotLoggedIn() {
    console.log("Returning not logged in render");
    return (
      <React.Fragment>
        <Header onLogin={(login) => this.handleLogin(login)} isLoggedIn={this.state.isLoggedIn}/>
        <Container>
          <Row><div>You're not logged in. Please log in first.</div></Row>
        </Container>
      </React.Fragment>
    );
  }

  renderDataNotReadyYet() {
    console.log("Returning data not ready yet render");
    return (
      <React.Fragment>
        <Header onLogin={(login) => this.handleLogin(login)} isLoggedIn={this.state.isLoggedIn}/>
        <Container className="loading-text-container">
          <Row><div className="loading-text">Loading data - please wait</div></Row>
          <Row><div className="slow-loading-text">It's taking longer than expected 🤔 Please wait a bit longer...</div></Row>
          <Row><div className="slower-loading-text">It's been way too long, try refreshing the page.</div></Row>
        </Container>
        </React.Fragment>
    );
  }

  renderMemoData(hasData) {
    console.log("Returning memo data render");
    return (
      <React.Fragment>
      <Header onLogin={(login) => this.handleLogin(login)} isLoggedIn={this.state.isLoggedIn}/>
      <Container>
      <Form className="MemoForm" onSubmit={(e) => this.onSubmit(e)}>
        <Row>
          <Form.Label>Title:</Form.Label>
          {/*<input type="text" name="memo_title"/>*/}
          <Form.Control type="text" name="memo_title" placeholder="Enter memo title"/>
        </Row>
        <Row>
          <Form.Label>Memo:</Form.Label>
          {/*<textarea name="memo_content"></textarea>*/}
          <Form.Control type="text" name="memo_content" placeholder="Enter memo content"/>
        </Row>
        <Row>
          <Button variant="primary" type="submit">Submit</Button>
        </Row>
      </Form>
      <Row>
        <div className="MemoTable">
        {hasData ? (<MemoTable onClick={(action, rowId) => this.handleClick(action, rowId)} data={this.state.data}/>) : (<></>)}
        </div>
      </Row>
    </Container>
    </React.Fragment>
    );
  }



  render() {
    console.log("Initial render (App.js)");
      if (!this.state.isLoggedIn) {
        return this.renderIsNotLoggedIn();
      }
      else if (this.state.data === null) {
        return this.renderDataNotReadyYet();
      }
      else {
        const hasData = this.state.data.length > 0;
        return this.renderMemoData(hasData);
      }
  }

}


App.defaultProps = {
  action: "./login",
  method: 'post'
}
