import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Alert from "react-bootstrap/Alert";
import './Login.css';

export default class Login extends React.Component {
 
    constructor(props) {
        super(props);
        this.wrongPasswordEntered = false;
    }

    toggleVisibility() {
        this.props.onToggleVisibility();
    }

    onSubmit(e, type) {
        console.log("login.js/OnSubmit activated")
        e.preventDefault();

        if (type === "login") {
            const formData = new FormData(e.target);
            const body = {};

            formData.forEach((value, property) => body[property] = value);
            console.table(body);

            fetch('./login', {
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/json',
                },
                body: JSON.stringify(body),
            })
            .then(response => response.json())
            .then(data => {
                // update App.state.isLoggedIn
                // data => { auth : boolean, username : string }
                // console.log(data);
                console.log(`Attempting to login as ${data.username}`);
                if (typeof data.auth === "boolean" && data.auth) {
                    console.log("Login success");
                    this.props.onLogin(data.auth);
                    this.wrongPasswordEntered = false;
                    this.toggleVisibility();
                }
                else {
                    console.log("Login failure");
                    this.wrongPasswordEntered = true;
                    this.props.onLogin(data.auth);
                }

            })
            .catch(err => console.log(err));
        }
        else if (type === "logout") {
            fetch('./logout', {
                method: 'GET'
            })
            .then(response => response.json())
            .then(data => {
                this.props.onLogin(data.auth);
            })
            .catch(err => console.log(err));
        }
    }

    render() {
        if (this.props.isLoggedIn) {
            return (
                <React.Fragment>
                    <Modal show={this.props.isShown} onHide={() => this.toggleVisibility()}>
    
                        <Modal.Header closeButton>
                            <Modal.Title>Logout of memo-app</Modal.Title>
                        </Modal.Header>
                        <Form className="loginForm" onSubmit={(e) => this.onSubmit(e, "logout")}>
                            <Modal.Body>
                                    <p>Do you wish to logout of memo-app?</p>
                            </Modal.Body>
    
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.toggleVisibility()}>Cancel</Button>
                                <Button variant="primary" type="submit" onClick={() => this.toggleVisibility()}>Logout</Button>
                            </Modal.Footer>
                        </Form>
    
                    </Modal>
                </React.Fragment>
            );
        }
        else {
            return (
                <React.Fragment>
                    <Modal show={this.props.isShown} onHide={() => this.toggleVisibility()}>
    
                        <Modal.Header closeButton>
                            <Modal.Title>Login to memo-app</Modal.Title>
                        </Modal.Header>
                        <Form className="loginForm" onSubmit={(e) => this.onSubmit(e, "login")}>
                            <Modal.Body>
                                    <Form.Label className="formLabel">Username</Form.Label>
                                    <Form.Control name="username" type="text" placeholder="Enter username"></Form.Control>
                                    <Form.Label className="formLabel">Password</Form.Label>
                                    <Form.Control name="password" type="password" placeholder="Enter password"></Form.Control>
                                    <a href="#forgot">Forgot your password?</a><br/>
                                    <a href="#tos">Terms and Conditions</a>
                                    <Alert show={this.wrongPasswordEntered} key="login-alert" variant="danger">Wrong password!</Alert>
                            </Modal.Body>
    
                            <Modal.Footer>
                                <Button variant="secondary" onClick={() => this.toggleVisibility()}>Cancel</Button>
                                <Button variant="primary" type="submit">Login</Button>
                            </Modal.Footer>
                        </Form>
    
                    </Modal>
                </React.Fragment>
            );
        }
        
    }
}