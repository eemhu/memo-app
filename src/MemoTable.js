import React from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import { Trash } from 'react-bootstrap-icons';

// should get data as a prop
// _id / title / content / priority
export default class MemoTable extends React.Component {
    constructor(props) {
        super(props);

        this.getKeys = this.getKeys.bind(this);
        this.getHeader = this.getHeader.bind(this);
        this.getRowsData = this.getRowsData.bind(this);
    }

    getKeys = function(){
        return Object.keys(this.props.data[0]);
    }

    getHeader = function(){
        var keys = this.getKeys();
        return keys.map((key, index) => {
            return <th key={key}>{key}</th>
        });
    }

    getRowsData = function(){
        var items = this.props.data;
        var keys = this.getKeys();

        var dataRows = items.map((row, index) => {
            console.log("dataRows: key=" + index + " data=" + row + " keys=" + keys);
            //console.table(row);

            let rowId = row["_id"];
            return (
                <tr key={index}>
                    <RenderRow key={index} data={row} keys={keys}/>
                    <td key={"r-" + index}><Button variant="secondary" onClick={() => this.handleClick("remove", rowId)}><Trash/>Remove</Button></td>
                    {/*<td key={"m-" + index}><Button variant="secondary" onClick={() => this.handleClick("modify", rowId)}>Send Modify</Button></td>*/}
                </tr>
            );
        });

        //console.log(typeof(dataRows));
        //console.log(dataRows);
        //dataRows.push(<tr key=''></tr>);
        return dataRows;
    }

    handleClick(action, rowId) {
        this.props.onClick(action, rowId);
    }
    
    render() {
        return (
            <div>
                <Table>
                    <thead>
                        <tr>{this.getHeader()}</tr>
                    </thead>
                    <tbody>
                        {this.getRowsData()}
                    </tbody>
                </Table>
            </div>
        );
    }
}

const RenderRow = (props) => {
    return props.keys.map((key, index) => {
        // console.log(key + " " + index + " " + props.data[key]);
        
        if (key === "title" || key === "content") {
            return <td key={props.data[key]}><textarea name={"memo_" + key} defaultValue={props.data[key]}></textarea></td>
        }
        else {
            return <td key={props.data[key]}>{props.data[key]}</td>
        }
        
    });
}