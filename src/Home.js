import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';


export default class App extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Container>
                    <Row><h1>Welcome to memo-app.</h1></Row>
                    <Row>You can use the header menus to navigate.</Row>
                </Container>
            </React.Fragment>
        );
    }
}